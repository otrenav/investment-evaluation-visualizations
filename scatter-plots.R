
source('./setup_data.R')
source('./plot-text.R')

library(plotly)

data <- setup_data()

sub_plot_per_year <- function(i, years, data) {
    year <- years[[i]]
    data_year <- data[data$year == year, ]
    m <- lm(SPX_pc ~ MU_pc, data = data_year)
    linear_model <- list(
        y = 10,
        x = min(data_year$MU_pc),
        font = list(size = 16, color = '#777777'),
        text = ~plot_text(year, data_year),
        showarrow = FALSE,
        xanchor = 'left',
        yanchor = 'top',
        xref = paste('x', i, sep = ""),
        yref = paste('y', i, sep = "")
    )
    p <- plot_ly(
        data_year,
        x = ~MU_pc,
        y = ~SPX_pc,
        text = ~date,
        type = 'scatter',
        mode = 'markers',
        showlegend = FALSE
    ) %>%
        add_lines(
            x = ~MU_pc,
            y = fitted(m),
            line = list(color = 'gray', width = 1)
        ) %>%
        layout(
            title = "Percentage Changes in Micron and S&P 500",
            yaxis = list (title = "Percentage change in Micron"),
            xaxis = list(title = "Percentage change in SPX"),
            annotations = linear_model
        )
    return(p)
}

years <- c("2015", "2016", "2017")
subplot(
    lapply(1:length(years), sub_plot_per_year, years, data),
    nrows=length(years),
    titleY = TRUE,
    titleX = TRUE
)
